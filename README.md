# mvn-repo

Библиотеки, которые не доступны нормальным путем через Maven repository и выложены вручную.


## Как добавить библиотеки?

Запустить следующую команду, заменив стандартные идертификаторы нужными значениями. pom.xml и файлы чексумм будут сгенерированы.

    mvn install:install-file -DgroupId=GROUP_ID -DartifactId=ARTIFACT_ID -Dversion=VERSION -Dfile=PATH_TO_JAR_FILE -Dpackaging=jar -DgeneratePom=true -DlocalRepositoryPath=. -DcreateChecksum=true

Добавить новый контент в Git:

    git add -A . && git commit -m "Добавлен GROUP_ID:ARTIFACT_ID:VERSION"

Загрузить изменения на сервер:

    git push origin repository


## Как пользоваться зависимостями?

В файле pom.xml проекта, которому нужны зависимости из этого репозитория добавить репозиторий: https://gitlab.com/fa13/mvn-repo/raw/master
